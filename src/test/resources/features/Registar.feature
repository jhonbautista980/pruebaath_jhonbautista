@CrearCuentaCorreo
Feature: Realizar Creacion de Cuenta de Correo

  @Scenario:CrearCuenta
  Scenario: Crear Cuenta Con datos Validos
    Given Como un usuario que no tiene cuenta de Email
    When Quiero crear una Cuenta Con mis datos personales Jhon Bautista jjBautistaAmaya2019
    And Un passwd correcto Barca2021
    Then Validar que la cuenta se pueda Crear

  @ScenarioOutline:CreacionMasivaDeCuenas
  Scenario Outline: Crear Cuenta Con datos Validos
    Given Como un usuario que no tiene cuenta de Email
    When Quiero crear una Cuenta Con mis datos personales <nombre> <apellido> <correo>
    And Un passwd correcto <clave>
    Then Validar que la cuenta se pueda Crear


    Examples:
      | nombre  | apellido    | correo            | clave      |
      | Juan    | Caballero   | correoJBPrueba001 | Prueba2021 |
      | Alonso  | Perez       | correoJBPrueba002 | Prueba2021 |
      | Jhon    | Castiblanco | correoJBPrueba004 | Prueba2021 |
      | Leonel  | Rocuzzo     | correoJBPrueba005 | Prueba2021 |
      | Luis    | Suarez      | correoJBPrueba006 | Prueba2021 |
      | Antonio | Caballero   | correoJBPrueba007 | Prueba2021 |