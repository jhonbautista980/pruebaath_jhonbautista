package Proyecto.Prueba.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp")
public class PruebaPageObject extends PageObject{
	
	/*
	 * Definicion de variables
	 * */
	String TXT_CONTRASENA="Loquequieraponer2020";
	@FindBy(id="firstName")
	public WebElementFacade TXT_NOMBRES;
	
	@FindBy(name="lastName")
	public WebElementFacade TXT_APELLIDOS;
	
	@FindBy(xpath="//*[@id=\"username\"]")
	public WebElementFacade TXT_USERNAME;
	
	@FindBy(name="Passwd")
	public WebElementFacade TXT_PASSWD;
	
	@FindBy(name="ConfirmPasswd")
	public WebElementFacade TXT_PASSWDCONF;
	
	@FindBy(id="accountDetailsNext")
	public WebElementFacade BTN_SIGUIENTE;
	
	@FindBy(id="headingText")
	public WebElementFacade LBL_CONFIRMACION;
	
	
	public void EscribirNombre(String vNombre) {

		TXT_NOMBRES.sendKeys(vNombre);
		}

	public void EscribirApellidos(String vApellido) {
		TXT_APELLIDOS.sendKeys(vApellido);
	}


	public void EscribirUsername(String vCorreo) {
		TXT_USERNAME.sendKeys(vCorreo);
		
	}


	public void EscribirPasswd(String vContraseña) {
		TXT_PASSWD.sendKeys(vContraseña);
		
	}


	public void EscribirRepasswd(String vContraseña) {
		TXT_PASSWDCONF.sendKeys(vContraseña);
		
	}

	public void InteractuarSiguiente() {
		BTN_SIGUIENTE.click();
		
	}

	

	
}
