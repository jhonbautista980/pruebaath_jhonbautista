package Proyecto.Prueba.Steps;

import Proyecto.Prueba.PageObject.PruebaPageObject;
import net.thucydides.core.annotations.Step;

public class PruebaSteps {
	PruebaPageObject PruebaPageObject;
	
	@Step
	public void IngresarCrearCuenta() {
		PruebaPageObject.open();
		
	}
	@Step
	public void DatosPersonales(String nombre, String apellido, String correo) {
		PruebaPageObject.EscribirNombre(nombre);
		PruebaPageObject.EscribirApellidos(apellido);
		PruebaPageObject.EscribirUsername(correo);
		
		
	}
	@Step
	public void Contrasena(String clave) {
		PruebaPageObject.EscribirPasswd(clave);
		PruebaPageObject.EscribirRepasswd(clave);
		PruebaPageObject.InteractuarSiguiente();		
	}
	@Step
	public void ValidacionCrearCuenta() {
		
		
	}
	

}
